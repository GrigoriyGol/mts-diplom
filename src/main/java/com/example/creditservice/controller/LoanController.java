package com.example.creditservice.controller;

import com.example.creditservice.controller.handler.GlobalExceptionHandler;
import com.example.creditservice.controller.model.request.DeleteRequest;
import com.example.creditservice.controller.model.request.OrderRequest;
import com.example.creditservice.controller.model.response.OrderResponse;
import com.example.creditservice.controller.model.response.StatusResponse;
import com.example.creditservice.controller.model.response.TariffsResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

public interface LoanController {
    @Operation(
            tags = "Получение тарифов",
            summary = "Метод получения тарифов",
            description = "Получить все данные из таблицы tariff и вернуть клиенту",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Получены доступные тарифы",
                            content = @Content(
                                    schema = @Schema(implementation = TariffsResponse.class),
                                    mediaType = MediaType.APPLICATION_JSON_VALUE)),
                    @ApiResponse(responseCode = "408", description = "Превышено время ожидания",
                            content = @Content(
                                    schema = @Schema(implementation = GlobalExceptionHandler.ErrorResponse.class),
                                    mediaType = MediaType.APPLICATION_JSON_VALUE))
            }
    )
    ResponseEntity<TariffsResponse> getTariffs();

    @Operation(
            tags = "Подача заявки",
            summary = "Метод подачи заявки на кредит",
            description = "Обработать запрос клиента и при возможности создать заявку",
            requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody(
                    description = "Тело запроса, которое включает в себя id клиента и id тарифа",
                    content = @Content(
                            schema = @Schema(implementation = OrderRequest.class))
            ),
            responses = {
                    @ApiResponse(responseCode = "200", description = "Заявка сохранена",
                            content = @Content(
                                    schema = @Schema(implementation = OrderResponse.class),
                                    mediaType = MediaType.APPLICATION_JSON_VALUE)),
                    @ApiResponse(responseCode = "400", description = "Произошла ошибка",
                            content = @Content(
                                    schema = @Schema(implementation = GlobalExceptionHandler.ErrorResponse.class),
                                    mediaType = MediaType.APPLICATION_JSON_VALUE)),
                    @ApiResponse(responseCode = "408", description = "Превышено время ожидания",
                            content = @Content(
                                    schema = @Schema(implementation = GlobalExceptionHandler.ErrorResponse.class),
                                    mediaType = MediaType.APPLICATION_JSON_VALUE))
            }
    )
    ResponseEntity<OrderResponse> makeOrder(@RequestBody OrderRequest orderRequest);

    @Operation(
            tags = "Получение статуса",
            summary = "Метод получения статуса заявки",
            description = "Обработать запрос клиента и вернуть статус заявки",
            parameters = {
                    @Parameter(
                            name = "orderId", required = true, description = "Идентификатор заявки",
                            allowEmptyValue = true,
                            schema = @Schema(
                                    type = "string",
                                    format = "uuid",
                                    description = "the generated UUID")
                    )},
            responses = {
                    @ApiResponse(responseCode = "200", description = "Получен статус заказа",
                            content = @Content(
                                    schema = @Schema(implementation = StatusResponse.class),
                                    mediaType = MediaType.APPLICATION_JSON_VALUE)),
                    @ApiResponse(responseCode = "400", description = "Произошла ошибка",
                            content = @Content(
                                    schema = @Schema(implementation = GlobalExceptionHandler.ErrorResponse.class),
                                    mediaType = MediaType.APPLICATION_JSON_VALUE)),
                    @ApiResponse(responseCode = "408", description = "Превышено время ожидания",
                            content = @Content(
                                    schema = @Schema(implementation = GlobalExceptionHandler.ErrorResponse.class),
                                    mediaType = MediaType.APPLICATION_JSON_VALUE))
            }
    )
    ResponseEntity<StatusResponse> getStatusOrder(@RequestParam String orderId);

    @Operation(
            tags = "Удаление заявки",
            summary = "Метод удаления заявки",
            description = "Обработать запрос клиента и при возможности удалить заявку",
            requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody(
                    description = "Тело запроса, которое включает в себя id клиента и id заявка",
                    content = @Content(
                            schema = @Schema(implementation = DeleteRequest.class))
            ),
            responses = {
                    @ApiResponse(responseCode = "200", description = "Заявка удалена"),
                    @ApiResponse(responseCode = "400", description = "Произошла ошибка",
                            content = @Content(
                                    schema = @Schema(implementation = GlobalExceptionHandler.ErrorResponse.class),
                                    mediaType = MediaType.APPLICATION_JSON_VALUE)),
                    @ApiResponse(responseCode = "408", description = "Превышено время ожидания",
                            content = @Content(
                                    schema = @Schema(implementation = GlobalExceptionHandler.ErrorResponse.class),
                                    mediaType = MediaType.APPLICATION_JSON_VALUE))
            }
    )
    ResponseEntity<?> deleteOrder(@RequestBody DeleteRequest deleteRequest);
}
