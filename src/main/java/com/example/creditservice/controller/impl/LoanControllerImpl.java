package com.example.creditservice.controller.impl;

import com.example.creditservice.controller.LoanController;
import com.example.creditservice.controller.model.request.DeleteRequest;
import com.example.creditservice.controller.model.request.OrderRequest;
import com.example.creditservice.controller.model.response.OrderResponse;
import com.example.creditservice.controller.model.response.StatusResponse;
import com.example.creditservice.controller.model.response.TariffsResponse;
import com.example.creditservice.service.LoanService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("loan-service")
@RequiredArgsConstructor
public class LoanControllerImpl implements LoanController {
    private final LoanService loanService;

    @GetMapping("getTariffs")
    public ResponseEntity<TariffsResponse> getTariffs() {
        return ResponseEntity.ok(new TariffsResponse(loanService.getAllTariffs()));
    }

    @PostMapping("order")
    public ResponseEntity<OrderResponse> makeOrder(@RequestBody OrderRequest orderRequest) {
        return ResponseEntity.ok(new OrderResponse(loanService.makeOrder(orderRequest.userId(), orderRequest.tariffId())
                .toString()));
    }

    @GetMapping("getStatusOrder")
    public ResponseEntity<StatusResponse> getStatusOrder(@RequestParam String orderId) {
        return ResponseEntity.ok(new StatusResponse(loanService.findStatusByOrderId(orderId)));
    }

    @DeleteMapping("deleteOrder")
    public ResponseEntity<?> deleteOrder(@RequestBody DeleteRequest deleteRequest) {
        loanService.deleteOrder(deleteRequest.userId(), deleteRequest.orderId());
        return ResponseEntity.ok().build();
    }
}
