package com.example.creditservice.controller.model.request;

public record OrderRequest(Long userId, Long tariffId) {
}
