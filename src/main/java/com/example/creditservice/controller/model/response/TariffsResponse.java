package com.example.creditservice.controller.model.response;

import com.example.creditservice.entity.Tariff;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;

import java.util.List;

@JsonTypeInfo(include = JsonTypeInfo.As.WRAPPER_OBJECT, use = JsonTypeInfo.Id.NAME)
@JsonTypeName("data")
public record TariffsResponse(List<Tariff> tariffs) {
}
