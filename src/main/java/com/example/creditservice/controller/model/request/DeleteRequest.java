package com.example.creditservice.controller.model.request;

public record DeleteRequest(Long userId, String orderId) {
}