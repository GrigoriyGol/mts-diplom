package com.example.creditservice.scheduler;

import com.example.creditservice.service.LoanService;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class LoanOrderScheduler {
    private final LoanService loanService;

    @Scheduled(initialDelay = 120000, fixedDelay = 120000)
    @Async
    public void updateStatusTask() {
        loanService.updateStatus();
    }
}
