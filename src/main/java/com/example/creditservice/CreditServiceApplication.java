package com.example.creditservice;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@EnableAsync
@EnableCircuitBreaker
@OpenAPIDefinition(info = @Info(
        title = "СЕРВИС ОФОРМЛЕНИЯ КРЕДИТА",
        version = "1.0.0",
        description = "Дипломный проект МТС FINTECH ACADEMY",
        contact = @Contact(url = "https://t.me/Grigalgol", name = "Gregory", email = "grisha101202@yandex.ru")
))
public class CreditServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(CreditServiceApplication.class, args);
    }
}
