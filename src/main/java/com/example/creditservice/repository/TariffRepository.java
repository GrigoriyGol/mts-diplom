package com.example.creditservice.repository;

import com.example.creditservice.entity.Tariff;

import java.util.List;
import java.util.Optional;

public interface TariffRepository {
    List<Tariff> getAllTariffs();

    Optional<Tariff> findTariffById(Long triggerId);
}
