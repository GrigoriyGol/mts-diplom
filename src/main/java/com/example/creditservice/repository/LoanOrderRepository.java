package com.example.creditservice.repository;

import com.example.creditservice.entity.LoanOrder;

import java.util.List;
import java.util.Optional;

public interface LoanOrderRepository {
    void saveLoanOrder(LoanOrder loanOrder);

    List<LoanOrder> getLoanOrdersByUserIdAndTariffId(Long userId, Long tariffId);

    Optional<String> findStatusByOrderId(String orderId);

    Optional<LoanOrder> findLoanOrderByUserIdAndOrderId(Long userId, String orderId);

    void deleteLoanOrderById(Long id);

    void updateStatus();
}
