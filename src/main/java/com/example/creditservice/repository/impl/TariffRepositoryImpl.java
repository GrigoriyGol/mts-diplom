package com.example.creditservice.repository.impl;

import com.example.creditservice.entity.Tariff;
import com.example.creditservice.repository.TariffRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class TariffRepositoryImpl implements TariffRepository {
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private static final String GET_ALL_TARIFFS = "SELECT * FROM tariff";
    private static final String FIND_TARIFF_BY_ID = "SELECT *" +
            " FROM tariff" +
            " WHERE tariff.id = :id";
    private final RowMapper<Tariff> rowMapper = (rs, rowNum) -> {
        Tariff tariff = new Tariff();
        tariff.setId(rs.getLong("id"));
        tariff.setType(rs.getString("type"));
        tariff.setInterestRate(rs.getString("interest_rate"));
        return tariff;
    };

    @Override
    public List<Tariff> getAllTariffs() {
        return jdbcTemplate.query(GET_ALL_TARIFFS, rowMapper);
    }

    @Override
    public Optional<Tariff> findTariffById(Long triggerId) {
        return jdbcTemplate
                .query(FIND_TARIFF_BY_ID, new MapSqlParameterSource("id", triggerId), rowMapper)
                .stream()
                .findFirst();
    }
}
