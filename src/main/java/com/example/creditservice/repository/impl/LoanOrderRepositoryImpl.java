package com.example.creditservice.repository.impl;

import com.example.creditservice.entity.LoanOrder;
import com.example.creditservice.repository.LoanOrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class LoanOrderRepositoryImpl implements LoanOrderRepository {
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private static final String SAVE_LOAD_ORDER = "INSERT INTO loan_order (order_id, user_id, tariff_id, " +
            "credit_rating, status, time_insert, time_update) " +
            "VALUES (:orderId, :userId, :tariffId, :creditRating, :status, :timeInsert, :timeUpdate)";
    private static final String FIND_LOAD_USER_BY_USER_ID_AND_TARIFF_ID = "SELECT *" +
            " FROM loan_order" +
            " WHERE user_id = :userId AND tariff_id = :tariffId";

    private static final String FIND_STATUS_BY_ORDER_ID = "SELECT status" +
            " FROM loan_order" +
            " WHERE order_id = :orderId";

    private static final String FIND_ORDER_BY_USER_ID_AND_ORDER_ID = "SELECT *" +
            " FROM loan_order" +
            " WHERE user_id = :userId AND order_id = :orderId";

    private static final String DELETE_ORDER_BY_USER_ID_AND_ORDER_ID = "DELETE FROM loan_order" +
            " WHERE id = :id";

    private static final String UPDATE_STATUS_AND_TIME_UPDATE = "UPDATE loan_order SET " +
            "    status = CASE WHEN random() < 0.5 THEN 'APPROVED' ELSE 'REFUSED' END," +
            "    time_update = now()" +
            " WHERE status = 'IN_PROGRESS'";

    private final RowMapper<LoanOrder> rowMapper = (rs, rowNum) -> {
        LoanOrder loanOrder = new LoanOrder();
        loanOrder.setId(rs.getLong("id"));
        loanOrder.setOrderId(rs.getString("order_id"));
        loanOrder.setUserId(rs.getLong("user_id"));
        loanOrder.setTariffId(rs.getLong("tariff_id"));
        loanOrder.setCreditRating(rs.getDouble("credit_rating"));
        loanOrder.setStatus(rs.getString("status"));
        loanOrder.setTimeInsert(rs.getTimestamp("time_insert"));
        loanOrder.setTimeUpdate(rs.getTimestamp("time_update"));
        return loanOrder;
    };

    @Override
    public void saveLoanOrder(LoanOrder loanOrder) {
        jdbcTemplate.update(SAVE_LOAD_ORDER, new MapSqlParameterSource()
                .addValue("orderId", loanOrder.getOrderId())
                .addValue("userId", loanOrder.getUserId())
                .addValue("tariffId", loanOrder.getTariffId())
                .addValue("creditRating", loanOrder.getCreditRating())
                .addValue("status", loanOrder.getStatus())
                .addValue("timeInsert", loanOrder.getTimeInsert())
                .addValue("timeUpdate", loanOrder.getTimeUpdate())
        );
    }

    @Override
    public List<LoanOrder> getLoanOrdersByUserIdAndTariffId(Long userId, Long tariffId) {
        return jdbcTemplate
                .query(FIND_LOAD_USER_BY_USER_ID_AND_TARIFF_ID, new MapSqlParameterSource()
                        .addValue("userId", userId).addValue("tariffId", tariffId), rowMapper);
    }

    @Override
    public Optional<String> findStatusByOrderId(String orderId) {
        return jdbcTemplate.queryForList(FIND_STATUS_BY_ORDER_ID,
                new MapSqlParameterSource("orderId", orderId),
                String.class).stream().findFirst();
    }

    @Override
    public Optional<LoanOrder> findLoanOrderByUserIdAndOrderId(Long userId, String orderId) {
        return jdbcTemplate
                .query(
                        FIND_ORDER_BY_USER_ID_AND_ORDER_ID,
                        new MapSqlParameterSource()
                                .addValue("userId", userId)
                                .addValue("orderId", orderId),
                        rowMapper).stream().findFirst();
    }

    @Override
    public void deleteLoanOrderById(Long id) {
        jdbcTemplate.update(DELETE_ORDER_BY_USER_ID_AND_ORDER_ID, new MapSqlParameterSource("id", id));
    }

    @Override
    public void updateStatus() {
        jdbcTemplate.update(UPDATE_STATUS_AND_TIME_UPDATE, new MapSqlParameterSource());
    }


}
