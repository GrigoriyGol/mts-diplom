package com.example.creditservice.service;

import com.example.creditservice.entity.Tariff;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;

import java.util.List;
import java.util.UUID;

public interface LoanService {
    List<Tariff> getAllTariffs();

    UUID makeOrder(Long userId, Long tariffId);

    String findStatusByOrderId(String orderId);

    void deleteOrder(Long userId, String orderId);

    void updateStatus();

}
