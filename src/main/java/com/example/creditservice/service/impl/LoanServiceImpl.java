package com.example.creditservice.service.impl;

import com.example.creditservice.entity.LoanOrder;
import com.example.creditservice.entity.Tariff;
import com.example.creditservice.exception.*;
import com.example.creditservice.repository.LoanOrderRepository;
import com.example.creditservice.repository.TariffRepository;
import com.example.creditservice.service.LoanService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class LoanServiceImpl implements LoanService {
    private final TariffRepository tariffRepository;
    private final LoanOrderRepository loanOrderRepository;

    @HystrixCommand(
            commandProperties = {
                    @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "1000")
            }
    )
    @Override
    public List<Tariff> getAllTariffs() {
        return tariffRepository.getAllTariffs();
    }

    @HystrixCommand(
            commandProperties = {
                    @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "1000")
            }
    )
    @Override
    public UUID makeOrder(Long userId, Long tariffId) {
        if (tariffRepository.findTariffById(tariffId).isEmpty())
            throw new TariffNotFoundException("Тариф не найден");

        List<LoanOrder> orders = loanOrderRepository
                .getLoanOrdersByUserIdAndTariffId(userId, tariffId);

        orders.forEach(
                loanOrder -> {
                    switch (loanOrder.getStatus()) {
                        case "IN_PROGRESS" -> throw new LoanConsiderationException("Заявка уже обрабатывается");
                        case "APPROVED" -> throw new LoanAlreadyApprovedException("Заявка уже одобрена");
                        case "REFUSED" -> {
                            Timestamp currentTime = new Timestamp(System.currentTimeMillis());
                            Timestamp timeUpdate = loanOrder.getTimeUpdate();
                            long diffInMillis = currentTime.getTime() - timeUpdate.getTime();
                            if (diffInMillis < 2 * 60 * 1000) throw new TryLaterException("Попробуйте немного позже");
                        }
                    }
                }
        );

        UUID orderId = UUID.randomUUID();
        Double creditRating = Math.floor((Math.random() * 81 + 10)) / 100.0;
        Timestamp currentTime = new Timestamp(System.currentTimeMillis());

        LoanOrder newLoanOrder = new LoanOrder(
                1L,
                orderId.toString(),
                userId,
                tariffId,
                creditRating,
                "IN_PROGRESS",
                currentTime,
                currentTime
        );

        loanOrderRepository.saveLoanOrder(newLoanOrder);
        return orderId;
    }

    @HystrixCommand(
            commandProperties = {
                    @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "1000")
            }
    )
    @Override
    public String findStatusByOrderId(String orderId) {
        Optional<String> statusOptional = loanOrderRepository.findStatusByOrderId(orderId);
        if (statusOptional.isEmpty()) throw new OrderNotFoundException("Заявка не найдена");
        else return statusOptional.get();
    }

    @HystrixCommand(
            commandProperties = {
                    @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "1000")
            }
    )
    @Override
    public void deleteOrder(Long userId, String orderId) {
        Optional<LoanOrder> loanOrderOptional = loanOrderRepository.findLoanOrderByUserIdAndOrderId(userId, orderId);
        if (loanOrderOptional.isEmpty()) throw new OrderNotFoundException("Заявка не найдена");
        if (!loanOrderOptional.get().getStatus().equals("IN_PROGRESS"))
            throw new OrderImpossibleToDeleteException("Невозможно удалить заявку");
        loanOrderRepository.deleteLoanOrderById(loanOrderOptional.get().getId());
    }

    @Override
    public void updateStatus() {
        loanOrderRepository.updateStatus();
    }
}
