package com.example.creditservice.exception;

public abstract class LoanServiceException extends RuntimeException {
    public abstract String getCode();

    public LoanServiceException(String message) {
        super(message);
    }
}
