package com.example.creditservice.exception;

import lombok.Getter;

@Getter
public class LoanConsiderationException extends LoanServiceException {
    private static final String CODE = "LOAN_CONSIDERATION";

    public LoanConsiderationException(String message) {
        super(message);
    }

    @Override
    public String getCode() {
        return CODE;
    }
}
