package com.example.creditservice.exception;

import lombok.Getter;

@Getter
public class TryLaterException extends LoanServiceException {
    private static final String CODE = "TRY_LATER";

    public TryLaterException(String message) {
        super(message);
    }

    @Override
    public String getCode() {
        return CODE;
    }
}
