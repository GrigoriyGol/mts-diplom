package com.example.creditservice.exception;

import lombok.Getter;

@Getter
public class TariffNotFoundException extends LoanServiceException {
    private static final String CODE = "TARIFF_NOT_FOUND";

    public TariffNotFoundException(String message) {
        super(message);
    }

    @Override
    public String getCode() {
        return CODE;
    }
}
