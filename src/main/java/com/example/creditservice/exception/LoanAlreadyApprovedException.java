package com.example.creditservice.exception;

import lombok.Getter;

@Getter
public class LoanAlreadyApprovedException extends LoanServiceException {
    private static final String CODE = "LOAN_ALREADY_APPROVED";

    public LoanAlreadyApprovedException(String message) {
        super(message);
    }

    @Override
    public String getCode() {
        return CODE;
    }
}
