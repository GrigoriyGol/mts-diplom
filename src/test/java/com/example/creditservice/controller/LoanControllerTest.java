package com.example.creditservice.controller;

import com.example.creditservice.controller.impl.LoanControllerImpl;
import com.example.creditservice.controller.handler.GlobalExceptionHandler;
import com.example.creditservice.controller.model.request.DeleteRequest;
import com.example.creditservice.controller.model.request.OrderRequest;
import com.example.creditservice.controller.model.response.OrderResponse;
import com.example.creditservice.controller.model.response.StatusResponse;
import com.example.creditservice.controller.model.response.TariffsResponse;
import com.example.creditservice.entity.LoanOrder;
import com.example.creditservice.entity.Tariff;
import com.example.creditservice.exception.*;
import com.example.creditservice.service.LoanService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(LoanControllerImpl.class)
public class LoanControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @MockBean
    private LoanService loanService;
    private static List<Tariff> tariffs;
    private static List<LoanOrder> loanOrders;

    @BeforeEach
    void setUp() {
        tariffs = Arrays.asList(
                new Tariff(1L, "CONSUMER", "11,5%"),
                new Tariff(2L, "MORTGAGE", "12,3%"),
                new Tariff(3L, "MICROLOAN", "18,4%")
        );
        loanOrders = Arrays.asList(
                new LoanOrder(1L, "f5a85cce-4f10-4865-a97c-ddc5602c0808", 1L, 1L, 0.8,
                        "IN_PROGRESS", new Timestamp(System.currentTimeMillis()),
                        new Timestamp(System.currentTimeMillis())),
                new LoanOrder(2L, "f5a85cce-4f10-4865-a97c-ddc5602c0809", 2L, 1L, 0.7,
                        "REFUSED", new Timestamp(System.currentTimeMillis()),
                        new Timestamp(System.currentTimeMillis())),
                new LoanOrder(3L, "f5a85cce-4f10-4865-a97c-ddc5602c0810", 3L, 2L, 0.6,
                        "APPROVED", new Timestamp(System.currentTimeMillis()),
                        new Timestamp(System.currentTimeMillis())),
                new LoanOrder(4L, "f5a85cce-4f10-4865-a97c-ddc5602c0811", 4L, 3L, 0.5,
                        "REFUSED", new Timestamp(System.currentTimeMillis()),
                        new Timestamp(System.currentTimeMillis() - 150000))
        );
    }

    @Test
    @SneakyThrows
    @DisplayName("Получение всех тарифов, ожидаемый рез-т - список тарифов")
    void testGetAllTariffs() {
        when(loanService.getAllTariffs()).thenReturn(tariffs);
        mockMvc
                .perform(
                        get("/loan-service/getTariffs")
                ).andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(new TariffsResponse(tariffs))));
    }

    @SneakyThrows
    @DisplayName("Получение статуса, когда заявка существует, ожидаемый рез-т - статус заявки")
    @Test
    void testFindStatusByOrderIdWhenOrderExists() {
        when(loanService.findStatusByOrderId(loanOrders.get(0).getOrderId())).thenReturn(loanOrders.get(0).getStatus());
        mockMvc
                .perform(
                        get("/loan-service/getStatusOrder")
                                .param("orderId", loanOrders.get(0).getOrderId())
                ).andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(new StatusResponse(loanOrders.get(0).getStatus()))));
    }

    @SneakyThrows
    @DisplayName("Получение статуса, когда заявка не существует, ожидаемый рез-т - исключение OrderNotFound")
    @Test
    void testFindStatusByOrderIdWhenOrderDoesNotExist() {
        OrderNotFoundException exception = new OrderNotFoundException("Заявка не найдена");
        when(loanService.findStatusByOrderId(anyString())).thenThrow(exception);
        mockMvc
                .perform(
                        get("/loan-service/getStatusOrder")
                                .param("orderId", loanOrders.get(0).getOrderId())
                ).andExpect(status().isBadRequest())
                .andExpect(content().json(objectMapper.writeValueAsString(new GlobalExceptionHandler.ErrorResponse(exception.getCode(), exception.getMessage()))));
    }

    @SneakyThrows
    @DisplayName("Удаление заявки, когда заявка не существует, ожидаемый рез-т - исключение OrderNotFound")
    @Test
    void testDeleteOrderWhenOrderDoesNotExist() {
        OrderNotFoundException exception = new OrderNotFoundException("Заявка не найдена");
        doThrow(exception).when(loanService).deleteOrder(anyLong(), anyString());
        mockMvc
                .perform(
                        delete("/loan-service/deleteOrder")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(new DeleteRequest(anyLong(), anyString())))
                ).andExpect(status().isBadRequest())
                .andExpect(content().json(objectMapper.writeValueAsString(new GlobalExceptionHandler.ErrorResponse(exception.getCode(), exception.getMessage()))));
        verify(loanService, times(1)).deleteOrder(anyLong(), anyString());
    }

    @SneakyThrows
    @DisplayName("Удаление заявки, когда заявка существует и она со статусом IN_PROGRESS," +
            " ожидаемый рез-т - удаленная заявка")
    @Test
    void testDeleteOrderWhenOrderExistsAndIsInProgress() {
        mockMvc
                .perform(
                        delete("/loan-service/deleteOrder")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(new DeleteRequest(loanOrders.get(0).getUserId(),
                                        loanOrders.get(0).getOrderId())))
                ).andExpect(status().isOk());
        verify(loanService, times(1)).deleteOrder(anyLong(), anyString());
    }

    @SneakyThrows
    @DisplayName("Удаление заявки, когда заявка существует и она не со статусом IN_PROGRESS," +
            " ожидаемый рез-т - исключение OrderImpossibleToDelete")
    @Test
    void testDeleteOrderWhenOrderExistsAndIsNotInProgress() {
        OrderImpossibleToDeleteException exception = new OrderImpossibleToDeleteException("Невозможно удалить заявку");
        doThrow(exception).when(loanService).deleteOrder(anyLong(), anyString());
        mockMvc
                .perform(
                        delete("/loan-service/deleteOrder")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(new DeleteRequest(anyLong(), anyString())))
                ).andExpect(status().isBadRequest())
                .andExpect(content().json(objectMapper.writeValueAsString(new GlobalExceptionHandler.ErrorResponse(exception.getCode(), exception.getMessage()))));
        verify(loanService, times(1)).deleteOrder(anyLong(), anyString());
    }

    @SneakyThrows
    @DisplayName("Создание заявки, когда тариф не существует, ожидаемый рез-т - исключение TariffNotFound")
    @Test
    void testMakeOrderWhenTariffNotFound() {
        TariffNotFoundException exception = new TariffNotFoundException("Тариф не найден");
        doThrow(exception).when(loanService).makeOrder(anyLong(), anyLong());
        mockMvc
                .perform(
                        post("/loan-service/order")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(new OrderRequest(anyLong(), anyLong())))
                ).andExpect(status().isBadRequest())
                .andExpect(content().json(objectMapper.writeValueAsString(new GlobalExceptionHandler.ErrorResponse(exception.getCode(), exception.getMessage()))));
        verify(loanService, times(1)).makeOrder(anyLong(), anyLong());
    }

    @SneakyThrows
    @DisplayName("Создание заявки, когда заявка уже существует и со статусом IN_PROGRESS," +
            " ожидаемый рез-т - исключение LoanConsideration")
    @Test
    void testMakeOrderWhenOrderInProgress() {
        LoanConsiderationException exception = new LoanConsiderationException("Заявка уже обрабатывается");
        doThrow(exception).when(loanService).makeOrder(anyLong(), anyLong());
        mockMvc
                .perform(
                        post("/loan-service/order")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(new OrderRequest(anyLong(), anyLong())))
                ).andExpect(status().isBadRequest())
                .andExpect(content().json(objectMapper.writeValueAsString(new GlobalExceptionHandler.ErrorResponse(exception.getCode(), exception.getMessage()))));
        verify(loanService, times(1)).makeOrder(anyLong(), anyLong());
    }

    @SneakyThrows
    @DisplayName("Создание заявки, когда заявка уже существует и со статусом APPROVED," +
            " ожидаемый рез-т - исключение LoanAlreadyApproved")
    @Test
    void testMakeOrderWhenOrderApproved() {
        LoanAlreadyApprovedException exception = new LoanAlreadyApprovedException("Заявка уже одобрена");
        doThrow(exception).when(loanService).makeOrder(anyLong(), anyLong());
        mockMvc
                .perform(
                        post("/loan-service/order")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(new OrderRequest(anyLong(), anyLong())))
                ).andExpect(status().isBadRequest())
                .andExpect(content().json(objectMapper.writeValueAsString(new GlobalExceptionHandler.ErrorResponse(exception.getCode(), exception.getMessage()))));
        verify(loanService, times(1)).makeOrder(anyLong(), anyLong());
    }

    @SneakyThrows
    @DisplayName("Создание заявки, когда заявка уже существует и со статусом REFUSED (время обновления меньше 2 минут)," +
            " ожидаемый рез-т - исключение TryLater")
    @Test
    void testMakeOrderWhenOrderRefusedAndTryLater() {
        TryLaterException exception = new TryLaterException("Попробуйте немного позже");
        doThrow(exception).when(loanService).makeOrder(anyLong(), anyLong());
        mockMvc
                .perform(
                        post("/loan-service/order")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(new OrderRequest(anyLong(), anyLong())))
                ).andExpect(status().isBadRequest())
                .andExpect(content().json(objectMapper.writeValueAsString(new GlobalExceptionHandler.ErrorResponse(exception.getCode(), exception.getMessage()))));
        verify(loanService, times(1)).makeOrder(anyLong(), anyLong());
    }

    @SneakyThrows
    @DisplayName("Создание заявки, когда заявка уже существует и со статусом REFUSED (время обновления больше 2 минут)," +
            " ожидаемый рез-т - создание заявки")
    @Test
    void testMakeOrderWhenOrderRefusedAndNoTryLater() {
        when(loanService.makeOrder(loanOrders.get(3).getUserId(), loanOrders.get(3).getTariffId()))
                .thenReturn(UUID.fromString(loanOrders.get(3).getOrderId()));
        mockMvc
                .perform(
                        post("/loan-service/order")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(new OrderRequest(loanOrders.get(3).getUserId(),
                                        loanOrders.get(3).getTariffId())))).andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(new OrderResponse(loanOrders.get(3).getOrderId()))));
        verify(loanService, times(1)).makeOrder(anyLong(), anyLong());
    }

    @SneakyThrows
    @DisplayName("Создание заявки, когда заявка не существует, ожидаемый рез-т - создание заявки")
    @Test
    void testMakeOrderWhenOrderDoesNotExist() {
        when(loanService.makeOrder(loanOrders.get(0).getUserId(), loanOrders.get(0).getTariffId()))
                .thenReturn(UUID.fromString(loanOrders.get(0).getOrderId()));
        mockMvc
                .perform(
                        post("/loan-service/order")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(new OrderRequest(loanOrders.get(0).getUserId(),
                                        loanOrders.get(0).getTariffId())))).andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(new OrderResponse(loanOrders.get(0).getOrderId()))));
        verify(loanService, times(1)).makeOrder(anyLong(), anyLong());
    }
}
