package com.example.creditservice.service;

import com.example.creditservice.entity.LoanOrder;
import com.example.creditservice.entity.Tariff;
import com.example.creditservice.exception.*;
import com.example.creditservice.repository.LoanOrderRepository;
import com.example.creditservice.repository.TariffRepository;
import com.example.creditservice.service.impl.LoanServiceImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class LoanServiceTest {

    @Mock
    private LoanOrderRepository loanOrderRepository;
    @Mock
    private TariffRepository tariffRepository;
    @InjectMocks
    private LoanServiceImpl loanService;
    private static List<Tariff> tariffs;
    private static List<LoanOrder> loanOrders;

    @BeforeAll
    static void setUp() {
        tariffs = Arrays.asList(
                new Tariff(1L, "CONSUMER", "11,5%"),
                new Tariff(2L, "MORTGAGE", "12,3%"),
                new Tariff(3L, "MICROLOAN", "18,4%")
        );
        loanOrders = Arrays.asList(
                new LoanOrder(1L, "f5a85cce-4f10-4865-a97c-ddc5602c0808", 1L, 1L, 0.8,
                        "IN_PROGRESS", new Timestamp(System.currentTimeMillis()),
                        new Timestamp(System.currentTimeMillis())),
                new LoanOrder(2L, "f5a85cce-4f10-4865-a97c-ddc5602c0809", 2L, 1L, 0.7,
                        "REFUSED", new Timestamp(System.currentTimeMillis()),
                        new Timestamp(System.currentTimeMillis())),
                new LoanOrder(3L, "f5a85cce-4f10-4865-a97c-ddc5602c0810", 3L, 2L, 0.6,
                        "APPROVED", new Timestamp(System.currentTimeMillis()),
                        new Timestamp(System.currentTimeMillis())),
                new LoanOrder(4L, "f5a85cce-4f10-4865-a97c-ddc5602c0811", 4L, 3L, 0.5,
                        "REFUSED", new Timestamp(System.currentTimeMillis()),
                        new Timestamp(System.currentTimeMillis() - 150000))
        );
    }

    @DisplayName("Получение всех тарифов, ожидаемый рез-т - список тарифов")
    @Test
    void testGetAllTariffs() {
        when(tariffRepository.getAllTariffs()).thenReturn(tariffs);
        assertEquals(tariffs, loanService.getAllTariffs());
    }

    @DisplayName("Получение статуса, когда заявка существует, ожидаемый рез-т - статус заявки")
    @Test
    void testFindStatusByOrderIdWhenOrderExists() {
        when(loanOrderRepository.findStatusByOrderId(loanOrders.get(0).getOrderId()))
                .thenReturn(Optional.ofNullable(loanOrders.get(0).getStatus()));
        when(loanOrderRepository.findStatusByOrderId(loanOrders.get(1).getOrderId()))
                .thenReturn(Optional.ofNullable(loanOrders.get(1).getStatus()));
        assertEquals(loanOrders.get(0).getStatus(), loanService.findStatusByOrderId(loanOrders.get(0).getOrderId()));
        assertEquals(loanOrders.get(1).getStatus(), loanService.findStatusByOrderId(loanOrders.get(1).getOrderId()));
    }

    @DisplayName("Получение статуса, когда заявка не существует, ожидаемый рез-т - исключение OrderNotFound")
    @Test
    void testFindStatusByOrderIdWhenOrderDoesNotExist() {
        when(loanOrderRepository.findStatusByOrderId(anyString())).thenReturn(Optional.empty());
        assertThrows(OrderNotFoundException.class, () -> {
            loanService.findStatusByOrderId(anyString());
        });
    }

    @DisplayName("Удаление заявки, когда заявка не существует, ожидаемый рез-т - исключение OrderNotFound")
    @Test
    void testDeleteOrderWhenOrderDoesNotExist() {
        when(loanOrderRepository.findLoanOrderByUserIdAndOrderId(anyLong(), anyString())).thenReturn(Optional.empty());
        assertThrows(OrderNotFoundException.class, () -> {
            loanService.deleteOrder(anyLong(), anyString());
        });
        verify(loanOrderRepository, never()).deleteLoanOrderById(anyLong());
    }

    @DisplayName("Удаление заявки, когда заявка существует и она со статусом IN_PROGRESS," +
            " ожидаемый рез-т - удаленная заявка")
    @Test
    void testDeleteOrderWhenOrderExistsAndIsInProgress() {
        when(loanOrderRepository.findLoanOrderByUserIdAndOrderId(loanOrders.get(0).getUserId(),
                loanOrders.get(0).getOrderId())).thenReturn(Optional.ofNullable(loanOrders.get(0)));
        loanService.deleteOrder(loanOrders.get(0).getUserId(), loanOrders.get(0).getOrderId());
        verify(loanOrderRepository, times(1)).deleteLoanOrderById(loanOrders.get(0).getId());
    }

    @DisplayName("Удаление заявки, когда заявка существует и она не со статусом IN_PROGRESS," +
            " ожидаемый рез-т - исключение OrderImpossibleToDelete")
    @Test
    void testDeleteOrderWhenOrderExistsAndIsNotInProgress() {
        when(loanOrderRepository.findLoanOrderByUserIdAndOrderId(loanOrders.get(1).getUserId(),
                loanOrders.get(1).getOrderId())).thenReturn(Optional.ofNullable(loanOrders.get(1)));
        assertThrows(OrderImpossibleToDeleteException.class, () -> {
            loanService.deleteOrder(loanOrders.get(1).getUserId(), loanOrders.get(1).getOrderId());
        });
        verify(loanOrderRepository, never()).deleteLoanOrderById(anyLong());
    }

    @DisplayName("Создание заявки, когда тариф не существует, ожидаемый рез-т - исключение TariffNotFound")
    @Test
    void testMakeOrderWhenTariffNotFound() {
        when(tariffRepository.findTariffById(anyLong())).thenReturn(Optional.empty());
        assertThrows(TariffNotFoundException.class, () -> {
            loanService.makeOrder(anyLong(), 100L);
        });
        verify(loanOrderRepository, never()).saveLoanOrder(any());
    }

    @DisplayName("Создание заявки, когда заявка уже существует и со статусом IN_PROGRESS," +
            " ожидаемый рез-т - исключение LoanConsideration")
    @Test
    void testMakeOrderWhenOrderInProgress() {
        when(tariffRepository.findTariffById(anyLong())).thenReturn(Optional.ofNullable(tariffs.get(0)));
        when(loanOrderRepository.getLoanOrdersByUserIdAndTariffId(loanOrders.get(0).getUserId(),
                loanOrders.get(0).getTariffId())).thenReturn(Collections.singletonList(loanOrders.get(0)));
        assertThrows(LoanConsiderationException.class, () -> {
            loanService.makeOrder(loanOrders.get(0).getUserId(), loanOrders.get(0).getTariffId());
        });
        verify(loanOrderRepository, never()).saveLoanOrder(any());
    }

    @DisplayName("Создание заявки, когда заявка уже существует и со статусом APPROVED," +
            " ожидаемый рез-т - исключение LoanAlreadyApproved")
    @Test
    void testMakeOrderWhenOrderApproved() {
        when(tariffRepository.findTariffById(anyLong())).thenReturn(Optional.ofNullable(tariffs.get(0)));
        when(loanOrderRepository.getLoanOrdersByUserIdAndTariffId(loanOrders.get(2).getUserId(),
                loanOrders.get(2).getTariffId())).thenReturn(Collections.singletonList(loanOrders.get(2)));
        assertThrows(LoanAlreadyApprovedException.class, () -> {
            loanService.makeOrder(loanOrders.get(2).getUserId(), loanOrders.get(2).getTariffId());
        });
        verify(loanOrderRepository, never()).saveLoanOrder(any());
    }

    @DisplayName("Создание заявки, когда заявка уже существует и со статусом REFUSED (время обновления меньше 2 минут)," +
            " ожидаемый рез-т - исключение TryLater")
    @Test
    void testMakeOrderWhenOrderRefusedAndTryLater() {
        when(tariffRepository.findTariffById(anyLong())).thenReturn(Optional.ofNullable(tariffs.get(0)));
        when(loanOrderRepository.getLoanOrdersByUserIdAndTariffId(loanOrders.get(1).getUserId(),
                loanOrders.get(1).getTariffId())).thenReturn(Collections.singletonList(loanOrders.get(1)));
        assertThrows(TryLaterException.class, () -> {
            loanService.makeOrder(loanOrders.get(1).getUserId(), loanOrders.get(1).getTariffId());
        });
        verify(loanOrderRepository, never()).saveLoanOrder(any());
    }

    @DisplayName("Создание заявки, когда заявка уже существует и со статусом REFUSED (время обновления больше 2 минут)," +
            " ожидаемый рез-т - создание заявки")
    @Test
    void testMakeOrderWhenOrderRefusedAndNoTryLater() {
        when(tariffRepository.findTariffById(anyLong())).thenReturn(Optional.ofNullable(tariffs.get(0)));
        when(loanOrderRepository.getLoanOrdersByUserIdAndTariffId(loanOrders.get(3).getUserId(),
                loanOrders.get(3).getTariffId())).thenReturn(Collections.singletonList(loanOrders.get(3)));
        loanService.makeOrder(loanOrders.get(3).getUserId(), loanOrders.get(3).getTariffId());
        verify(loanOrderRepository, times(1)).saveLoanOrder(any());
    }

    @DisplayName("Создание заявки, когда заявка не существует, ожидаемый рез-т - создание заявки")
    @Test
    void testMakeOrderWhenOrderDoesNotExist() {
        when(tariffRepository.findTariffById(anyLong())).thenReturn(Optional.ofNullable(tariffs.get(0)));
        when(loanOrderRepository.getLoanOrdersByUserIdAndTariffId(anyLong(),
                anyLong())).thenReturn(Collections.emptyList());
        loanService.makeOrder(anyLong(), 1L);
        verify(loanOrderRepository, times(1)).saveLoanOrder(any());
    }

    @DisplayName("Обновление статуса заявок, ожидаемый рез-т - обновление заявки")
    @Test
    void testUpdateStatus() {
        loanService.updateStatus();
        verify(loanOrderRepository, times(1)).updateStatus();
    }
}
