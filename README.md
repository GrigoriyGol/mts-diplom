# MTS diplom

## Выпускной проект МТС Финтех Академии

- Разработка сервиса "ОФОРМЛЕНИЕ КРЕДИТА"

## Бизнес требования

- Клиент должен иметь возможность получить тарифы и условия кредита

- Клиент должен иметь возможность подать заявку на кредит

- Клиент должен иметь возможность получить статус заявки на кредит

- Клиент должен иметь возможность удалить заявку на кредит

## Используемые технологии

- Java
- Spring Boot
- PostgreSQL
- Liquibase
- JdbcTemplate
- JUnit 5
- Swagger
- Lombok
- Maven
- Circuit Breaker
- Docker

## API

- Метод получения тарифов
    - GET loan-service/getTariffs


- Метод подачи заявки на кредит
    - POST loan-service/order
    - Входящие данные { "userId": {userId}, "tariffId": {tariffId} }


- Метод получения статуса заявки
    - GET loan-service/getStatusOrder?orderId={orderId}


- Метод удаления заявки
    - DELETE loan-service/deleteOrder
    - Входящие данные { "userId": {userId}, "orderId": {orderId} }


- Документация API представлена с помощью Swagger и доступна по url /swagger-ui/index.html#/
- Протестировать функционал API можно непосредственно через Swagger или с помощью Postman или curl

## Как запустить проект?
1) Для запуска проекта Вам необходимо установить Docker на устройство с официального сайта (https://www.docker.com/)
2) В заранее выбранную папку необходимо склонить проект
   - Clone with SSH: git@gitlab.com:GrigoriyGol/mts-diplom.git
   - Clone with HTTPS: https://gitlab.com/GrigoriyGol/mts-diplom.git
3) Если вы используете ОС MacOS - перед запуском нужно в терминале прописать команду "chmod +x ./mvnw"
4) В терминале прописать команду docker-compose up

